<?php


namespace Apeisia\BusinessLogBundle\Log;


use Apeisia\BusinessLogBundle\Entity\BusinessLog;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BusinessLogger
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var bool
     */
    private $runInCommand = false;

    public function __construct(TokenStorageInterface $tokenStorage, EntityManagerInterface $em)
    {
        $this->em           = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function log(string $action, $object, $context = null, $objectClass = null, ?string $accountName = null)
    {
        $token = $this->tokenStorage->getToken();
        $login = $account = 'Unbekannt';
        if ($token) {
            $login = $token->getUser();
            if ($login instanceof AbstractLogin) {
                $account = $login->getCurrentAccount();
            }
        } elseif ($this->runInCommand) {
            $login = $account = 'Hintergrundprozess';
        }

        if ($accountName) {
            // force a specific account name for the log
            $account = $accountName;
        }

        $log = new BusinessLog($login, $account, $action, $object, $context, $objectClass);
        $this->em->persist($log);

        return $log;
    }

    /**
     * @return bool
     */
    public function isRunInCommand(): bool
    {
        return $this->runInCommand;
    }

    /**
     * @param bool $runInCommand
     */
    public function setRunInCommand(bool $runInCommand): void
    {
        $this->runInCommand = $runInCommand;
    }
}
