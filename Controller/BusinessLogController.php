<?php

namespace Apeisia\BusinessLogBundle\Controller;

use Apeisia\BaseBundle\Crud\CrudController;
use Apeisia\BusinessLogBundle\Entity\BusinessLog;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/businesslog", name="businesslog_")
 * @Security("is_granted('ROLE_BUSINESS_LOG')")
 */
class BusinessLogController extends CrudController
{

    /**
     * @param Request $request
     *
     * @Rest\Get("/")
     *
     * @return \FOS\RestBundle\View\View
     */
    public function listAction(Request $request)
    {
        return $this->doIndex($request);
    }

    protected function getEntityName()
    {
        return BusinessLog::class;
    }

    protected function getForm($entity): FormInterface
    {
        throw new \Exception('not used');
    }

    protected function getListUser()
    {
        return null;
    }
}
