<?php

namespace Apeisia\BusinessLogBundle\Repository;

use Apeisia\BaseBundle\Crud\AbstractSearchRepository;

class BusinessLogRepository extends AbstractSearchRepository
{

    protected function getSearchFields()
    {
        return [
            'accountName',
            'objectName',
            'context',
        ];
    }

    public function findForObject($object, $objectClass=null)
    {
        if(!$objectClass) {
            $objectClass = get_class($object);
        }
        
        return $this->createQueryBuilder('a')
            ->where('a.objectClass = :objectClass')
            ->andWhere('a.objectId = :id')
            ->orderBy('a.loggedAt', 'desc')
            ->setParameter('objectClass', $objectClass)
            ->setParameter('id', $object->getId())
            ->getQuery()
            ->execute()
        ;
    }
    

}
