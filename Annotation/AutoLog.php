<?php


namespace Apeisia\BusinessLogBundle\Annotation;

/**
 * @Annotation
 */
class AutoLog
{
    public $create = true;
    public $update = true;
    public $remove = true;
}
