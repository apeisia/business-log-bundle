<?php

namespace Apeisia\BusinessLogBundle\EventListener;

use Apeisia\BusinessLogBundle\Annotation\AutoLog;
use Apeisia\BusinessLogBundle\Log\BusinessLogger;
use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

class EntityListener
{

    const ACTION_CREATE = 'create';
    const ACTION_UPDATE = 'update';
    const ACTION_REMOVE = 'remove';

    /**
     * @var Reader
     */
    private $annotationReader;
    /**
     * @var BusinessLogger
     */
    private $logger;

    public function __construct(Reader $annotationReader, BusinessLogger $logger)
    {
        $this->annotationReader = $annotationReader;
        $this->logger           = $logger;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->log(self::ACTION_UPDATE, $args->getObject(), $args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->log(self::ACTION_CREATE, $args->getObject(), $args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->log(self::ACTION_REMOVE, $args->getObject(), $args);
    }

    private function log($action, $object, LifecycleEventArgs $ea)
    {
        [$annotation, $objectClass] = $this->getAnnotation($object);
        if (!$annotation) return;
        if ($action == self::ACTION_CREATE && !$annotation->create) return;
        if ($action == self::ACTION_UPDATE && !$annotation->update) return;
        if ($action == self::ACTION_REMOVE && !$annotation->remove) return;
        if($action == self::ACTION_REMOVE) {
            $objectClass = null;
        }
        if ($action == self::ACTION_CREATE || $action == self::ACTION_REMOVE) {
            $log = $this->logger->log($action, $object, null, $objectClass);
        } else {
            throw new \Exception('not implemented yet');
        }

        $em  = $ea->getEntityManager();
        $uow = $em->getUnitOfWork();
        $uow->computeChangeSet($em->getClassMetadata(get_class($log)), $log);
        $em->flush();
    }

    private function getAnnotation($object)
    {
        $class = new \ReflectionClass($object);
        while ($class) {
            foreach ($this->annotationReader->getClassAnnotations($class) as $annotation) {
                if ($annotation instanceof AutoLog) {
                    return [$annotation, $class->getName()];
                }
            }
            $class = $class->getParentClass();
        }

        return [null, null];
    }
}
