<?php

namespace Apeisia\BusinessLogBundle\EventListener;

use Apeisia\BusinessLogBundle\Entity\BusinessLog;
use Apeisia\LoginAccess\Service\RelationConfiguration;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\ClassMetadata;

class DoctrineMetadataSubscriber implements EventSubscriber
{
    private $relationConfiguration;

    public function __construct(RelationConfiguration $relationConfiguration)
    {
        $this->relationConfiguration = $relationConfiguration;
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::loadClassMetadata,
        ];
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        /** @var ClassMetadata $metaData */
        $metaData = $eventArgs->getClassMetadata();

        if ($metaData->getName() != BusinessLog::class) {
            return;
        }

        $metaData->mapManyToOne([
            'fieldName'    => 'login',
            'targetEntity' => $this->relationConfiguration->loginClass,
            'joinColumns'  => [
                [
                    'name'                 => 'login_id',
                    'unique'               => false,
                    'nullable'             => true,
                    'onDelete'             => 'SET NULL',
                    'columnDefinition'     => NULL,
                    'referencedColumnName' => 'id',
                ],
            ],
        ]);
        $metaData->mapManyToOne([
            'fieldName'    => 'account',
            'targetEntity' => $this->relationConfiguration->accountClass,
            'joinColumns'  => [
                [
                    'name'                 => 'account_id',
                    'unique'               => false,
                    'nullable'             => true,
                    'onDelete'             => 'SET NULL',
                    'columnDefinition'     => NULL,
                    'referencedColumnName' => 'id',
                ],
            ],
        ]);

    }
}
