<?php

namespace Apeisia\BusinessLogBundle\Entity;

use Apeisia\BaseBundle\Entity\EntityUUId;
use Apeisia\BusinessLogBundle\Repository\BusinessLogRepository;
use Apeisia\LoginAccess\Entity\AbstractAccount;
use Apeisia\LoginAccess\Entity\AbstractLogin;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Proxy\Proxy;
use JMS\Serializer\Annotation as Serializer;
use ReflectionClass;

#[
    ORM\Entity(repositoryClass: BusinessLogRepository::class),
    ORM\Table(name: "business_log"),
    ORM\Index(columns: ["object_name", "object_id"], name: "object_idx")
]
class BusinessLog
{
    use EntityUUId;

    /**
     * Relation defined in DoctrineMetadataSubscriber
     */
    private AbstractLogin $login;

    /**
     * @Serializer\Groups({"list"})
     */
    #[ORM\Column(type: "string", nullable: false)]
    #[Serializer\Groups(['list'])]
    private ?string $loginName = null;

    /**
     * Relation defined in DoctrineMetadataSubscriber
     */
    private AbstractAccount $account;

    #[ORM\Column(type: "string", nullable: false)]
    #[Serializer\Groups(['list'])]
    private ?string $accountName = null;

    #[ORM\Column(type: "string", nullable: true)]
    #[Serializer\Groups(['list'])]
    private ?string $objectId = null;

    #[ORM\Column(type: "string", nullable: true)]
    #[Serializer\Groups(['list'])]
    private ?string $objectName = null;

    #[ORM\Column(type: "string", nullable: true)]
    #[Serializer\Groups(['list'])]
    private ?string $objectClass = null;

    #[ORM\Column(type: "string", nullable: false)]
    #[Serializer\Groups(['list'])]
    private ?string $action;

    #[ORM\Column(type: "datetime", nullable: false)]
    #[Serializer\Groups(['list'])]
    private ?DateTime $loggedAt;

    #[ORM\Column(type: "json", nullable: true)]
    #[Serializer\Groups(['list'])]
    private mixed $context;

    public function __construct($login, $account, string $action, $object, $context, $objectClass = null)
    {
        $this->action   = $action;
        $this->context  = $context;
        $this->loggedAt = new DateTime();

        if ($login instanceof AbstractLogin) {
            $this->login     = $login;
            $this->loginName = $login->getUsername();
        } elseif (is_string($login)) {
            $this->loginName = $login;
        }
        if ($account instanceof AbstractAccount) {
            $this->account     = $account;
            $this->accountName = (string)$account;
        } elseif (is_string($account)) {
            $this->accountName = $account;
        }
        if ($object) {
            if ($objectClass) {
                $this->objectClass = $objectClass;
            } else {
                if ($object instanceof Proxy) {
                    $ref               = new ReflectionClass($object);
                    $this->objectClass = $ref->getParentClass()->getName();
                } else {
                    $this->objectClass = get_class($object);
                }
            }
            if (method_exists($object, 'getId')) {
                $this->objectId = $object->getId();
            }
            if (method_exists($object, '__toString')) {
                $this->objectName = $object->__toString();
            }
        }
    }

    public function getLogin(): ?AbstractLogin
    {
        return $this->login;
    }

    public function getLoginName(): string
    {
        return $this->loginName;
    }

    public function getAccount(): ?AbstractAccount
    {
        return $this->account;
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }

    public function getObjectId(): ?string
    {
        return $this->objectId;
    }

    public function getObjectName(): ?string
    {
        return $this->objectName;
    }

    public function getObjectClass(): ?string
    {
        return $this->objectClass;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getLoggedAt(): DateTime
    {
        return $this->loggedAt;
    }

    public function getContext(): mixed
    {
        return $this->context;
    }
}
